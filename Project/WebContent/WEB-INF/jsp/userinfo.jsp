<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
	<body>
        <div class="col-4 mx-auto">
        <h><p class="bg-primary">ユーザー名
           <form class="form-signin" action="LogoutServlet" method="get">
            <input type="submit" onclick="location.href='./login.html'" value="ログアウト" class="btn btn-primary" class="btn page-link text-dark d-inline-block" ></form></p></h>
        <h1>ユーザー情報詳細参照</h1>
		<p>ログインID  ${userdetail.loginId} </p>
		<p>ユーザ名	 ${userdetail.name}</p>
		<p>生年月日  ${userdetail.birthDate}</p>
		<p>登録日時  ${userdetail.createDate}</p>
		<p>更新日時  ${userdetail.birthDate}</p>
       <form class="form-signin" action="UserListServlet" method="get">
             <h3><input type="submit" onclick="location.href='./userList.html'" value="戻る" class="btn btn-primary" class="btn page-link text-dark d-inline-block"></h3>
             </form>
        </div>
	</body>
</html>
