<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
	<body>
		 <form class="form-signin" action="LoginServlet" method="post">
        <div class="col-4 mx-auto">
            <h1>ログイン画面</h1>
            <h2>ログインID</h2>
                <input type="text"  name="loginId" class="btn page-link text-dark d-inline-block" style="width;200px", >
            <h3>パスワード</h3>
                <input type="text" name="password" style="width;200px," class="btn page-link text-dark d-inline-block">
                <input type="submit" onclick="location.href='./userList.html'" value="ログイン" class="btn page-link text-dark d-inline-block">
         <p> ${errMsg} </p>
        </div></form>

    </body>
</html>
