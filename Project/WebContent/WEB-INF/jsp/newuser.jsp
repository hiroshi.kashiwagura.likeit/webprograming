<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
	<body>
        <div class="col-4 mx-auto">

        <h><p class="bg-primary">ユーザー名
        <form class="form-signin" action="LogoutServlet" method="get">
            <input type="submit" onclick="location.href='./login.html'" value="ログアウト" class="btn btn-primary" class="btn page-link text-dark d-inline-block" ></form>
		 <form class="form-signin" action="NewUserServlet" method="post">
		<h1>ユーザー新規登録</h1>
		 <p> ${errMsg} </p>
        <p>ログインID
            <input type="text" name="loginid" class="btn page-link text-dark d-inline-block"></p>
        <p>パスワード
            <input type="password" name="password"class="btn page-link text-dark d-inline-block"></p>
        <p>パスワード（確認）
            <input type="password" name="password1" class="btn page-link text-dark d-inline-block"></p>
        <p>ユーザー名
            <input type="text" name="user_name" class="btn page-link text-dark d-inline-block"></p>
        <p>生年月日
            <input type="date" name="birth_date" class="btn page-link text-dark d-inline-block"></p>

                <h2><input type="submit" onclick="location.href='./index.html'" value="登録" class="btn page-link text-dark d-inline-block"></h2>
            </form>
            <form class="form-signin" action="UserListServlet" method="get">
             <h3><input type="submit" onclick="location.href='./userList.html'" value="戻る" class="btn btn-primary" class="btn page-link text-dark d-inline-block"></h3>
             </div></form>

		</div>
    </body>
</html>
