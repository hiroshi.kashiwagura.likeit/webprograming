<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
	<body>
        <div class="col-4 mx-auto">
        <h><p class="bg-primary">ユーザー名
               <form class="form-signin" action="LogoutServlet" method="get">
            <input type="submit" onclick="location.href='./login.html'" value="ログアウト" class="btn btn-primary" class="btn page-link text-dark d-inline-block" ></form>
		<h1>ユーザー削除確認</h1>
	    <p>ログインID：${userdetail.name}
            を本当に削除してよろしいでしょうか。</p>
        <form class="form-signin" action="UserListServlet" method="get">
             <h3><input type="submit" onclick="location.href='./userList.html'" value="キャンセル" class="btn btn-primary" class="btn page-link text-dark d-inline-block"></h3>
             </form>
        <form class="form-signin" action="UserDeleteServlet" method="post">
        <input type="hidden" name="id" value="${userdetail.id}">
        <input type="submit" onclick="location.href='./index.html'" value="OK" class="btn page-link text-dark d-inline-block"></form>
            </div>
    </body>
</html>