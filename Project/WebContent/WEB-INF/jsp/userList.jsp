<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
	<body>

        <div class="col-4 mx-auto">
        <p class="bg-primary">ユーザー名
           <form class="form-signin" action="LogoutServlet" method="get">
            <input type="submit" onclick="location.href='./login.html'" value="ログアウト" class="btn btn-danjer" ></form></p>

		<h2>ユーザー一覧</h2>

		<form class="form-signin" action="NewUserServlet" method="get">
         <input type="submit" onclick="location.href='./newuser.html'" value="新規登録" class="btn btn-primary" class="btn page-link text-dark d-inline-block">
        </form>

           <form class="form-signin" action="UsrListServlet" method="post">
            <p>ログインID</p>
            <input type="text" name="loginId" style="width;200px," class="btn page-link text-dark d-inline-block">
            <p>ユーザー名</p>
            <input type="text" name="userName" style="width;200px," class="btn page-link text-dark d-inline-block">
        <p>生年月日
            <input type="date" name="startDate" style="width;200px," class="btn page-link text-dark d-inline-block">
              ~
            <input type="date" name="endDate" style="width;200px," class="btn page-link text-dark d-inline-block">  </p>
        <input type="submit" onclick="location.href='./inde.html'" value="検索" class="btn page-link text-dark d-inline-block">
		</form>


		<table border="1">
		<tr>
		<th>ログインID</th>
		<th>ユーザ名</th>
		<th>生年月日</th>
		<th></th>
        </tr>
         <c:forEach var="user" items="${userList}" >
		<tr>
         <td>${user.loginId}</td>
         <td>${user.name}</td>
         <td>${user.birthDate}</td>

         <td>
           <c:if test="${userInfo.id ==1}" >
           <a href="UserDetailServlet?id=${user.id}" class="btn btn-primary" >詳細</a>
          <a href="UserUpdateServlet?id=${user.id}" class="btn btn-success" >更新</a>
           <a href="UserDeleteServlet?id=${user.id}" class="btn btn-danger" >削除</a></c:if>


            <c:if test="${userInfo.id !=1}" >
			<a href="UserDetailServlet?id=${user.id}" class="btn btn-primary" >詳細</a></c:if>
         	<c:if test="${userInfo.loginId == user.loginId}" >
          <a href="UserUpdateServlet?id=${user.id}" class="btn btn-success" >更新</a></c:if>

               </td>
		</tr>
		</c:forEach>

            </table></div>


	</body>

</html>
