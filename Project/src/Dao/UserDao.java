package Dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM userInfo WHERE login_id = ? and password = ?";


			Encryption en =new Encryption();
			String pass=en.encryption(password);

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, pass);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            int idData = rs.getInt("id");

            return new User(loginIdData, nameData,idData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM userInfo where id !=1";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findResearch(String loginIdP, String userNameP, String startDateP ,String endDateP) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM userInfo where id != 1";

            if(!(userNameP.equals(""))) {
            	sql += " AND name like '%" + userNameP + "%'";
            }
            if(!(loginIdP.equals(""))) {
            	sql += " AND login_id = '" + loginIdP + "'";
            }
            if(!(startDateP.equals(""))) {
            	sql += " AND birth_date >= '" + startDateP + "'";
            }
            if(!(endDateP.equals(""))) {
            	sql += " AND birth_date <= '" + endDateP + "'";
            }
             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }



//    public List<User> findByUserInfo(String loginId, String userName, String startDate ,String endDate) {
//        Connection conn = null;
//        List<User> userList = new ArrayList<User>();
//        try {
//            // データベースへ接続
//            conn = DBManager.getConnection();
//
//            // SELECT文を準備
//            String sql = "SELECT * FROM userInfo WHERE login_id = ? and name like '%?%' and birth_date =< ? and birth_date =>?";
//
//             // SELECTを実行し、結果表を取得
//            PreparedStatement pStmt = conn.prepareStatement(sql);
//            pStmt.setString(1, loginId);
//            pStmt.setString(2, userName);
//            pStmt.setString(3, startDate);
//            pStmt.setString(4, endDate);
//            ResultSet rs = pStmt.executeQuery();
//
//            if (!rs.next()) {
//
//                String loginid = rs.getString("login_id");
//                String name = rs.getString("name");
//                Date birthDate = rs.getDate("birth_date");
//                User user = new User(loginid, name, birthDate);
//
//                userList.add(user);
//
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            // データベース切断
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//        }
//        return userList;
//    }

    public void addUser(String loginId, String password, String password1, String user_name, String birth_date) {
    	Connection conn = null;
    		try {
    			// データベースへ接続
    			conn = DBManager.getConnection();
    			// INSERT文を準備
    			String sql = "INSERT INTO userinfo (login_id,password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,now(),now())";
    			Encryption en =new Encryption();
    			String pass=password=en.encryption(password);

    			PreparedStatement stmt = conn.prepareStatement(sql);
    			stmt.setString(1,loginId );
    			stmt.setString(2,pass );
    			stmt.setString(3,user_name);
    			stmt.setString(4,birth_date );

    			int result = stmt.executeUpdate();
    			// 追加された行数を出力
    			System.out.println(result);
    			stmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			} finally {
    				if (conn != null) {
    	                try {
    	                    conn.close();
    	                } catch (SQLException e) {
    	                    e.printStackTrace();
    	                }
    	            }

    			}

    }
    public User userdetail(String id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM userInfo WHERE id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);

            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int Id = rs.getInt("id");
            String loginId2 = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            return  new User(Id, loginId2, name, birthDate, password, createDate, updateDate);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
    public int UpdateUserInfo(String Name,String Password,String date,String id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String  sql = "UPDATE userinfo SET name=?,password=?,birth_date=? WHERE Id=?";
            Encryption en =new Encryption();
			en.encryption(Password);
			String pass=en.encryption(Password);

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, Name);
            pStmt.setString(2, pass);
            pStmt.setString(3, date);
            pStmt.setString(4, id);
            int rs = pStmt.executeUpdate();

            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            return 2;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 2;
                }
            }
        }
    }
    public int DeleteUserInfo(String id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String  sql = "delete from userinfo  WHERE Id=?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);
            int rs = pStmt.executeUpdate();

            return rs;


        } catch (SQLException e) {
            e.printStackTrace();
            return 2;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 2;
                }
            }
        }
    }
    public User loginidsearch(String loginid) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM userInfo WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginid);

            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int Id = rs.getInt("id");
            String loginId2 = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            return  new User(Id, loginId2, name, birthDate, password, createDate, updateDate);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
}