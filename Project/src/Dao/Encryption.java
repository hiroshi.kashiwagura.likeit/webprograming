package Dao;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;


public class Encryption {

 public String encryption(String pass) {
	 try {
		//ハッシュを生成したい元の文字列
		 String source = pass;
		 //ハッシュ生成前にバイト配列に置き換える際のCharset
		 Charset charset = StandardCharsets.UTF_8;
		 //ハッシュアルゴリズム
		 String algorithm = "MD5";

		 //ハッシュ生成処理
		 byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		 String result = DatatypeConverter.printHexBinary(bytes);

		 return result;

     } catch ( NoSuchAlgorithmException  e) {
         e.printStackTrace();

         	return null;

     }
 }

}
